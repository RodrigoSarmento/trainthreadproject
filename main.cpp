#include <QApplication>
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QGraphicsView>
#include <QPen>
#include <QTimer>
#include <QThread>
#include <QSlider>
#include <QHBoxLayout>
#include "trainInfo.h"
#include "trainUtils.h"
#include "trainMovement.h"
#include "trainThread.h"
#include "sliderThread.h"


#include "main.moc"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    // Create a QGraphicsScene to hold the items
    QGraphicsScene scene;

    // Create the first square (yellow)
    qreal x1 = 50.0;
    qreal y1 = 50.0;
    qreal size1 = 100.0;
    QGraphicsRectItem square1(x1, y1, size1, size1);
    square1.setPen(QPen(Qt::yellow, 5));
    scene.addItem(&square1);

    TrainInfo train1;
    train1.train_index = 0;
    train1.train = new QGraphicsRectItem(x1, y1 - 6, size1 / 3, size1 / 8);
    train1.train->setBrush(Qt::yellow);
    scene.addItem(train1.train);
    generatePositionsHorizontally(train1.lanes[0], x1-25, x1+25, 0);
    generatePositionsVertically(train1.lanes[1],x1-15+(size1/2), y1-25,y1+25);
    generatePositionsHorizontally(train1.lanes[2],x1-25,x1+25, y1+50,true);
    generatePositionsVertically(train1.lanes[3], x1-(size1/2)-15, y1-25,y1+25,true);

    // Create the second square (blue)
    qreal x2 = 151.0;
    qreal y2 = 50.0;
    qreal size2 = 100.0;
    QGraphicsRectItem square2(x2, y2, size2, size2);
    square2.setPen(QPen(Qt::blue, 5));
    scene.addItem(&square2);

    TrainInfo train2;
    train2.train_index = 1;
    train2.train = new QGraphicsRectItem(x2, y2 - 6, size2 / 3, size2 / 8);
    train2.train->setBrush(Qt::blue);
    scene.addItem(train2.train);
    generatePositionsHorizontally(train2.lanes[0], x1-50, x1+40, 0);
    generatePositionsVertically(train2.lanes[1],x1+35, y2-25,y2+25);
    generatePositionsHorizontally(train2.lanes[2],x1-50,x1+45, y2+50,true);
    generatePositionsVertically(train2.lanes[3], x1-65, y2-25,y2+25,true);

    // Create the third square (red)
    qreal x3 = 50.0;
    qreal y3 = 155.0;
    qreal size3 = 100.0;
    QGraphicsRectItem square3(x3, y3, size3, size3);
    square3.setPen(QPen(Qt::red, 5));
    scene.addItem(&square3);

    TrainInfo train3;
    train3.train_index = 2;
    train3.train = new QGraphicsRectItem(x3, y3 - 6, size3 / 3, size3 / 8);
    train3.train->setBrush(Qt::red);
    scene.addItem(train3.train);
    generatePositionsHorizontally(train3.lanes[0], x3-50, x3+40, 0);
    generatePositionsVertically(train3.lanes[1],x3+35, y1-25,y1+25);
    generatePositionsHorizontally(train3.lanes[2],x3-50,x3+45, y1+50,true);
    generatePositionsVertically(train3.lanes[3], x3-65, y1-25,y1+25,true);

    // Create the fourth square (green)
    qreal x4 = 151.0;
    qreal y4 = 155.0;
    qreal size4 = 100.0;
    QGraphicsRectItem square4(x4, y4, size4, size4);
    square4.setPen(QPen(Qt::green, 5));
    scene.addItem(&square4);

    TrainInfo train4;
    train4.train_index = 3;
    train4.train = new QGraphicsRectItem(x4, y4 - 6, size4 / 3, size4 / 8);
    train4.train->setBrush(Qt::green);
    scene.addItem(train4.train);
    generatePositionsHorizontally(train4.lanes[0], x1-45, x1+45, 0);
    generatePositionsVertically(train4.lanes[1],x1+35, y1-35,y1+35);
    generatePositionsHorizontally(train4.lanes[2],x1-50,x1+25, y1+50,true);
    generatePositionsVertically(train4.lanes[3], x1-65, y1-35,y1+35,true);

    // Creating TrainMovement instance creating the train info
    TrainMovement trainMovement1(&train1);
    // Creates the Thread with the Train Movement argument
    TrainThread trainThread1(&trainMovement1, 50);

    // Establish connection between TrainThread and Train Moved and create a signal to connect with the scene. This is needed
    // since the scene can only be changed in the main thread
    QObject::connect(&trainThread1, SIGNAL(trainMoved()), &scene, SLOT(update()));

    TrainMovement trainMovement2(&train2);
    TrainThread trainThread2(&trainMovement2,100);
    QObject::connect(&trainThread2, SIGNAL(trainMoved()), &scene, SLOT(update()));

    TrainMovement trainMovement3(&train3);
    TrainThread trainThread3(&trainMovement3,30);
    QObject::connect(&trainThread3, SIGNAL(trainMoved()), &scene, SLOT(update()));

    TrainMovement trainMovement4(&train4);
    TrainThread trainThread4(&trainMovement4,200);
    QObject::connect(&trainThread4, SIGNAL(trainMoved()), &scene, SLOT(update()));

    QWidget mainWindow;
    QHBoxLayout layout(&mainWindow);

    QSlider slideBar1;
    slideBar1.setOrientation(Qt::Vertical);
    slideBar1.setRange(0, 100);
    slideBar1.setValue(TrainUtils::convertTimeToScale(100));
    slideBar1.setStyleSheet("QSlider::groove:vertical {"
                         "margin: 5px 0px;"
                         "}"
                         "QSlider::handle:vertical {"
                         "background: yellow;"
                         "border: 1px solid yellow;"
                         "width: 10px;"
                         "margin: -2px -2px;"
                         "border-radius: 5px;"
                         "}");
    layout.addWidget(&slideBar1);

    QSlider slideBar2;
    slideBar2.setOrientation(Qt::Vertical);
    slideBar2.setRange(0, 100);
    slideBar2.setValue(TrainUtils::convertTimeToScale(50));
    slideBar2.setStyleSheet("QSlider::groove:vertical {"
                            "margin: 5px 0px;"
                            "}"
                            "QSlider::handle:vertical {"
                            "background: blue;"
                            "border: 1px solid blue;"
                            "width: 10px;"
                            "margin: -2px -2px;"
                            "border-radius: 5px;"
                            "}");
    layout.addWidget(&slideBar2);

    QSlider slideBar3;
    slideBar3.setOrientation(Qt::Vertical);
    slideBar3.setRange(0, 100);
    slideBar3.setValue(TrainUtils::convertTimeToScale(30));
    slideBar3.setStyleSheet("QSlider::groove:vertical {"
                            "margin: 5px 0px;"
                            "}"
                            "QSlider::handle:vertical {"
                            "background: red;"
                            "border: 1px solid red;"
                            "width: 10px;"
                            "margin: -2px -2px;"
                            "border-radius: 5px;"
                            "}");
    layout.addWidget(&slideBar3);

    QSlider slideBar4;
    slideBar4.setOrientation(Qt::Vertical);
    slideBar4.setRange(0, 100);
    slideBar4.setValue(TrainUtils::convertTimeToScale(200));
    slideBar4.setStyleSheet("QSlider::groove:vertical {"
                            "margin: 5px 0px;"
                            "}"
                            "QSlider::handle:vertical {"
                            "background: green;"
                            "border: 1px solid green;"
                            "width: 10px;"
                            "margin: -2px -2px;"
                            "border-radius: 5px;"
                            "}");
    layout.addWidget(&slideBar4);

    QList<int> velocities = {50, 100, 30, 200};

    // Connection between the sliderThread and sliderBar, this is going to update the sliderThread data.
    SliderThread sliderThread(velocities);
    QObject::connect(&slideBar1, SIGNAL(valueChanged(int)), &sliderThread, SLOT(updateVelocity1(int)));
    QObject::connect(&slideBar2, SIGNAL(valueChanged(int)), &sliderThread, SLOT(updateVelocity2(int)));
    QObject::connect(&slideBar3, SIGNAL(valueChanged(int)), &sliderThread, SLOT(updateVelocity3(int)));
    QObject::connect(&slideBar4, SIGNAL(valueChanged(int)), &sliderThread, SLOT(updateVelocity4(int)));

    // Connection between sliderThread and trainThreads
    QObject::connect(&sliderThread, SIGNAL(velocityUpdated(int,int)), &trainThread1, SLOT(handleVelocityUpdated(int,int)), Qt::DirectConnection);
    QObject::connect(&sliderThread, SIGNAL(velocityUpdated(int,int)), &trainThread2, SLOT(handleVelocityUpdated(int,int)), Qt::DirectConnection);
    QObject::connect(&sliderThread, SIGNAL(velocityUpdated(int,int)), &trainThread3, SLOT(handleVelocityUpdated(int,int)), Qt::DirectConnection);
    QObject::connect(&sliderThread, SIGNAL(velocityUpdated(int,int)), &trainThread4, SLOT(handleVelocityUpdated(int,int)), Qt::DirectConnection);

    // Start the threads
    trainThread1.start();
    trainThread2.start();
    trainThread3.start();
    trainThread4.start();


    // Create a QGraphicsView to visualize the scene
    QGraphicsView view(&scene);
    view.setFixedSize(1000, 1000);  // Set the desired width and height
    layout.addWidget(&view);
    view.show();

    // Set the layout on the main window
    mainWindow.setLayout(&layout);
    mainWindow.show();

    return app.exec();
}
