#include "trainMovement.h"
#include "trainUtils.h"
#include "trainInfo.h"
#include <QDebug>

int returnInWitchSignalIs(int trainIndex,int trainPosition){
    switch(trainIndex){
        case 0: {
            if(trainPosition == 1){
                return 3;
            } else if(trainPosition == 2){
                return 4;
            }
            return 0;
            break;
        }
        case 1: {
            if (trainPosition == 2){
                return 7;
            } else if (trainPosition == 3){
                return 3;
            }
            return 0;
            break;
        }
        case 2:{
            if (trainPosition == 0){
                return 4;
            } else if(trainPosition == 1){
                return 9;
            }
            return 0;
            break;
        }
        case 3:{
            if (trainPosition == 0){
                return 7;
            } else if(trainPosition == 3){
                return 9;
            }
            return 0;
            break;
        }
    }
}

//Construtor
TrainMovement::TrainMovement(TrainInfo* train)
    : train(train)
{
}

void TrainMovement::updateTrainTime(int newTime){
    emit trainTimeUpdated();
}

void TrainMovement::updateTrainPosition()
{
    int nextLane = train->currentLane;
    int nextPosition = train->currentPosition+1;

    if (train->currentLane == 0 || train->currentLane == 2) {
        train->train->setRotation(0);
    } else {
        train->train->setRotation(90);
    }
    train->train->setTransformOriginPoint(train->train->boundingRect().center());

    if (nextPosition >= 10) {
        nextPosition = 0;

        nextLane++;
        if (nextLane >= 4) {
            nextLane = 0;
        }
    }

    // Convert absolute position to relative signal
    int nextSignal  = returnInWitchSignalIs(train->train_index, nextLane);
    int currentSignal = returnInWitchSignalIs(train->train_index,train->currentLane);

    if(train->train_index == 0){
       // qDebug() << "Sou o trem amarelo e estou na posição " << train->currentLane << " Indo para " << nextLane;
    }

    if(nextLane == train->currentLane){
        // If I'm not changing lane keep the lane occupied
        setLaneOccupied(nextSignal,true);
    }else{
        // If I'm trying to change lane, I need to check if I can
        if (isLaneOccupied(nextSignal)){
            return;
        }else{
            // Since I can change lane, set the previously lane free and make the next lane occupied
            setLaneOccupied(currentSignal, false);
            setLaneOccupied(nextSignal, true);
        }

    }

    train->currentLane = nextLane;
    train->currentPosition = nextPosition;

    TrainUtils::moveTrain(train->train, train->lanes[nextLane][nextPosition].x, train->lanes[nextLane][nextPosition].y);


    emit trainPositionUpdated();
}

TrainInfo* TrainMovement::getTrain() const
{
    return train;
}
