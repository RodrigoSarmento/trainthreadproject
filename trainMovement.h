#ifndef TRAINMOVEMENT_H
#define TRAINMOVEMENT_H

#include <QObject>
#include "trainInfo.h"
#include "trainUtils.h"

class TrainMovement : public QObject {
    Q_OBJECT
public:
    TrainMovement(TrainInfo* train);

public slots:
    void updateTrainPosition();
    void updateTrainTime(int newTime);

signals:
    void trainPositionUpdated();
    void trainTimeUpdated();

public:
    TrainInfo* getTrain() const;

private:
    TrainInfo* train;
};

#endif // TRAINMOVEMENT_H
