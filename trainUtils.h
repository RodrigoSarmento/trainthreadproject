#ifndef TRAINUTILS_H
#define TRAINUTILS_H

#include <QGraphicsRectItem>

class TrainUtils {
public:
    static void moveTrain(QGraphicsRectItem* train, qreal x, qreal y);
    static int convertTimeToScale(int time);
    static int convertScaleToTime(int scale);
};

#endif // TRAINUTILS_H
