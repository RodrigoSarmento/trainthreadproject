#ifndef TRAINTHREAD_H
#define TRAINTHREAD_H

#include <QObject>
#include <QThread>
#include <QTimer>
#include "trainMovement.h"

class TrainThread : public QThread
{
    Q_OBJECT
public:
    explicit TrainThread(TrainMovement* trainMovement,int time);

protected:
    void run() override;

public slots:
    void handleVelocityUpdated(int index, int time);

signals:
    void trainMoved();

private:
    TrainMovement* trainMovement;
    QTimer* timer;
};

#endif // TRAINTHREAD_H
