#include "trainThread.h"
#include <QDebug>
#include <QTimer>

QTimer* timer = nullptr;

TrainThread::TrainThread(TrainMovement* trainMovement, int time)
    : trainMovement(trainMovement), timer(nullptr)
{
    timer = new QTimer(this);

    connect(timer, SIGNAL(timeout()), trainMovement, SLOT(updateTrainPosition()));

    timer->setInterval(time);
    timer->start();
}


void TrainThread::run()
{
    exec();
}

void TrainThread::handleVelocityUpdated(int index, int velocity)
{
    if (index == this->trainMovement->getTrain()->train_index){
        int time = TrainUtils::convertScaleToTime(velocity);

        timer->setInterval(time);
    }

}
