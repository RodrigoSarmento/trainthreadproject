#ifndef TRAIN_H
#define TRAIN_H

#include <QGraphicsRectItem>

struct Position {
    qreal x;
    qreal y;
};

struct TrainInfo {
    QGraphicsRectItem* train;
    Position lanes[4][10]; // Matrix that defines the lane and position for each lane
    int currentLane;  // Current lane index
    int currentPosition;  // Current position index in the current lane
    int train_index;

    TrainInfo(int initialLane = 0, int initialPosition = 0)
        : currentLane(initialLane), currentPosition(initialPosition) {}
};

void generatePositionsHorizontally(Position (&lane)[10], qreal minX, qreal maxX, qreal y, bool reverse = false);
void generatePositionsVertically(Position (&lane)[10], qreal x, qreal minY, qreal maxY, bool reverse = false);
bool isLaneOccupied(int lane);
void setLaneOccupied(int lane, bool occupied);

#endif // TRAIN_H
