#include "TrainUtils.h"
#include <QDebug>

void TrainUtils::moveTrain(QGraphicsRectItem* train, qreal x, qreal y) {
    train->setPos(x, y);
}

int TrainUtils::convertTimeToScale(int time){
    time = std::max(30, std::min(200, time));

    // Reverse the conversion proportionally
    int reversedValue = ((200 - time) * 100) / 170;

    return reversedValue;
}

int TrainUtils::convertScaleToTime(int scale) {
    // Define the source range [30, 200]
    scale = std::max(0, std::min(100, scale));

    int convertedValue = 200 -((scale * 170) / 100);

    return convertedValue;
}
