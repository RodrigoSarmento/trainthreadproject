#ifndef SLIDERTHREAD_H
#define SLIDERTHREAD_H

#include <QObject>
#include <QThread>
#include <QList>

class SliderThread: public QThread
{
    Q_OBJECT

public:
    explicit SliderThread(const QList<int>& velocities);

protected:
    void run() override;

private slots:
    void updateVelocity(int index, int velocity);
    void updateVelocity1(int velocity);
    void updateVelocity2(int velocity);
    void updateVelocity3(int velocity);
    void updateVelocity4(int velocity);

signals:
    void velocityUpdated(int index, int velocity);

private:
    QList<int> velocities;

};

#endif // SLIDERTHREAD_H
