#include "sliderThread.h"
#include <QDebug>

SliderThread::SliderThread(const QList<int>& velocities)
{
    this->velocities = velocities;

}

void SliderThread::run()
{

    exec();
}

void SliderThread::updateVelocity(int index, int velocity)
{
    if (index >= 0 && index < velocities.size()) {
        velocities[index] = velocity;
        emit velocityUpdated(index, velocity);
    }
}


void SliderThread::updateVelocity1(int velocity)
{
    this->updateVelocity(0,velocity);

}

void SliderThread::updateVelocity2(int velocity)
{
    this->updateVelocity(1,velocity);
}

void SliderThread::updateVelocity3(int velocity)
{
    this->updateVelocity(2,velocity);
}

void SliderThread::updateVelocity4(int velocity)
{
    this->updateVelocity(3,velocity);
}
