#include "trainInfo.h"
#include <QMutexLocker>

QMap<int, bool> laneOccupancy;
QMutex L3Lane, L4Lane, L7Lane, L9Lane;

void generatePositionsHorizontally(Position (&lane)[10], qreal minX, qreal maxX, qreal y, bool reverse) {
    qreal step = (maxX - minX) / 9.0;  // Calculate the step size between positions

    if (reverse) {
        for (int i = 9; i >= 0; --i) {
            lane[i].x = minX + (9 - i) * step;  // Set the x-coordinate of the position
            lane[i].y = y;  // Set the y-coordinate of the position
        }
    } else {
        for (int i = 0; i < 10; ++i) {
            lane[i].x = minX + i * step;  // Set the x-coordinate of the position
            lane[i].y = y;  // Set the y-coordinate of the position
        }
    }
}

void generatePositionsVertically(Position (&lane)[10], qreal x, qreal minY, qreal maxY, bool reverse) {
    qreal step = (maxY - minY) / 9.0;  // Calculate the step size between positions

    if (reverse) {
        for (int i = 9; i >= 0; --i) {
            lane[i].x = x;  // Set the x-coordinate of the position
            lane[i].y = minY + (9 - i) * step;  // Set the y-coordinate of the position
        }
    } else {
        for (int i = 0; i < 10; ++i) {
            lane[i].x = x;  // Set the x-coordinate of the position
            lane[i].y = minY + i * step;  // Set the y-coordinate of the position
        }
    }
}


bool isLaneOccupied(int lane)
{
    switch(lane){
        case 3: {
            QMutexLocker locker(&L3Lane);
            return laneOccupancy.value(0, false);
            break;
        }
        case 4: {
            QMutexLocker locker(&L4Lane);
            return laneOccupancy.value(1, false);
            break;
        }
        case 7: {
            QMutexLocker locker(&L7Lane);
            return laneOccupancy.value(2, false);
            break;
        }
        case 9: {
            QMutexLocker locker(&L9Lane);
            return laneOccupancy.value(3, false);
            break;
        }
        default: return false;
    }
}

void setLaneOccupied(int lane, bool occupied)
{
    switch(lane)
    {
        case 3: {
                QMutexLocker locker(&L3Lane);
                laneOccupancy[0] = occupied;
                break;
        }
        case 4: {
                QMutexLocker locker(&L4Lane);
                laneOccupancy[1] = occupied;
                break;
        }
        case 7: {
                QMutexLocker locker(&L7Lane);
                laneOccupancy[2] = occupied;
                break;
        }
        case 9: {
                QMutexLocker locker(&L9Lane);
                laneOccupancy[3] = occupied;
                break;
        }
        default: break;
    }
}


